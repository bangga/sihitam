using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using bri.pdam.model;
using MySql.Data.MySqlClient;
using System.IO;
using System.Data.OleDb;


/// <summary>
/// Summary description for dbutils
/// </summary>
/// 
namespace bri.pdam.db
{
    public class dbutils
    {
        private MySqlConnection sqlCon;
        private AppSettingsReader vAppSettingsR;
        private String hostdb;
        private String uname;
        private String pass;
        private String dbname;
        private String connStr;


        public dbutils()
        {
            vAppSettingsR = new AppSettingsReader();
            hostdb = vAppSettingsR.GetValue("hostdb", Type.GetType("System.String")).ToString();
            uname = vAppSettingsR.GetValue("uname", Type.GetType("System.String")).ToString();
            pass = vAppSettingsR.GetValue("pass", Type.GetType("System.String")).ToString();
            dbname = vAppSettingsR.GetValue("dbname", Type.GetType("System.String")).ToString();

            connStr = createConnStr(hostdb, uname, pass, dbname);
            sqlCon = new MySqlConnection(connStr);
        }

        public String createConnStr(String vHostDB, String vUser, String vPass, String vDbname)
        {
            String connStr = "server=" + vHostDB + ";database=" + vDbname + ";user id=" + vUser + ";password=" + vPass + ";pooling=false";
            return connStr;
        }

        public int nonQuerySQL(String cmdsql)
        {
            logging("Trying to run query: " + cmdsql);
            int x = -1;
            try
            {
                sqlCon.Open();
                MySqlCommand command = new MySqlCommand();
                command.Connection = sqlCon;
                command.CommandText = cmdsql;
                x = command.ExecuteNonQuery();
                sqlCon.Close();
            }
            catch (Exception ex)
            {
                logging(ex.Message);
                sqlCon.Close();
            }
            return x;
        }

        public DataTable getDataTable(string cmdSQL)
        {
            logging("Trying to run query: " + cmdSQL);
            DataTable dTable = new DataTable();
            MySqlDataAdapter sqlDA = new MySqlDataAdapter();
            try
            {
                sqlCon.Open();
                MySqlCommand command = new MySqlCommand();
                command.CommandText = cmdSQL;
                command.Connection = sqlCon;
                sqlDA.SelectCommand = command;
                sqlDA.Fill(dTable);
                sqlCon.Close();
            }
            catch (Exception ex)
            {
                logging(ex.Message);
                sqlCon.Close();
            }

            return dTable;
        }

        public DataTable getPesan()
        {
            DataTable dt = new DataTable();
            dt = this.getDataTable("SELECT Pesan FROM pdam_bbri.pesan_footer");
            return dt;        
        }

        public DataTable inquiryTagihan(String nosal)
        {
            DataTable dt = new DataTable();
            //dt = this.getDataTable("SELECT A.pel_no, A.rek_uangair, A.rek_adm, A.rek_meter, A.rek_denda, A.rek_angsuran, B.pel_nama, B.pel_alamat, B.gol_kode, A.rek_angs_ket FROM pdam_bbri.tm_rekening A, pdam_bbri.tm_pelanggan B WHERE A.pel_no = '" + nosal + "' AND rek_sts = 1 AND rek_byr_sts = 0 AND A.pel_no = B.pel_no;");
            dt = this.getDataTable("SELECT A.pel_no, A.rek_uangair, A.rek_adm, A.rek_meter, A.rek_denda, A.rek_angsuran, A.pel_nama, A.pel_alamat, A.gol_kode, A.rek_angs_ket  FROM pdam_bbri.tm_rekening A WHERE A.pel_no = '" + nosal + "' AND rek_sts = 1 AND rek_byr_sts = 0;");
            int jml = dt.Rows.Count;
            return dt;
        }

        public bool bayarTagihan(dataPush request)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");  
            int a = -1;
            bool result = false;
           
            DataTable dt = new DataTable();
            dt = this.getDataTable("SELECT A.pel_no,  A.rek_nomor, A.rek_uangair, A.rek_adm, A.rek_meter, A.rek_denda, A.rek_angsuran, A.pel_nama, A.pel_alamat, A.gol_kode, A.rek_angs_ket  FROM pdam_bbri.tm_rekening A WHERE A.pel_no = '" + request.Nosal + "' AND rek_sts = 1 AND rek_byr_sts = 0;");
            int jml = dt.Rows.Count;
            if (jml == 0)
            {
                result = false;
            }
            else
            {
                a = this.nonQuerySQL("CALL pdam_bbri.bbri_bayar_rek('" + request.SeqNumber + "', '" + request.TanggalBayar + "', '" + request.ReffNumber + "', '" + dt.Rows[0]["rek_nomor"] + "', '" + request.UserID + "', '" + request.IpAddress + "', '" + request.NamaCabang + "', '" + request.NominalTagihan + "', '" + request.CetakanKeN + "', '" + request.JamBayar + "', '1');");
                if (a != -1)
                {
                    result = true;
                }
                else
                    result = false;
            }

            //DataTable dt = new DataTable();
            //dt = this.getDataTable("SELECT A.pel_no, A.rek_nomor, A.rek_uangair, A.rek_adm, A.rek_meter, A.rek_denda, A.rek_angsuran, A.rek_angs_ket, B.pel_nama, B.pel_alamat, B.gol_kode, A.rek_angs_ket FROM pdam_bbri.tm_rekening A, pdam_bbri.tm_pelanggan B WHERE A.pel_no = '" + request.Nosal + "' AND rek_sts = 1 AND rek_byr_sts = 0 AND A.pel_no = B.pel_no;");
            //int jml = dt.Rows.Count;
            //if (jml == 0)
            //{
            //   result = false;
            //}
            //else
            //{
            //    a = this.nonQuerySQL("UPDATE pdam_bbri.tm_rekening SET rek_byr_sts = '1' WHERE tm_rekening.pel_no = '" + request.Nosal + "' ");
               
               
            //   if (a != -1)
            //   {
            //       int b = this.nonQuerySQL("INSERT INTO pdam_bbri.tm_pembayaran VALUES ('" + request.SeqNumber + "', '" + request.TanggalBayar + "', '" + request.ReffNumber + "', '" + dt.Rows[0]["rek_nomor"] + "', '" + request.UserID + "', '" + request.IpAddress + "', '" + request.NamaCabang + "', '" + request.NominalTagihan + "', '" + request.CetakanKeN + "', '" + request.JamBayar + "', '1');");
            //       if (b != -1)
            //       {
            //           result = true;
            //       }

            //       else
            //           result = false;                             
            //   }
            //   else
            //       result = false;
            //}

            
            return result;
        }

        public void logging(string text)
        {

            string fullpath = @"D:\LOGGING\queryResultPdam.txt";

            StreamWriter logwriter = new StreamWriter(new FileStream(fullpath, FileMode.Append, FileAccess.Write));
            logwriter.WriteLine(text);
            logwriter.Close();
        }

    }
}